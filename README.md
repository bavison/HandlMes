Handel's Messiah
================
Arranged for Maestro
--------------------

Acorn shipped a number of example files for their Maestro music application with every machine. Two of these were the first two (half) movements of George Frideric Handel's celebrated oratorio _The Messiah_. Being something of a completist, I always wanted the music to continue to the end of the piece. So I continued where Acorn left off, and entered the remaining movements. This is a considerable amount of music - a full performance of _The Messiah_ usually takes 2-3 hours.

This undertaking could simply have remained in my personal archives indefinitely, but I think it's time I shared it more widely. It is hereby made available under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

For those that care about such things, there are many different editions of _The Messiah_, and they can't even agree about how the movements are numbered. This arrangement is based on the Novello edition.

Ben Avison, February 2021
